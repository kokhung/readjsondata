//
//  ViewController.swift
//  ReadJsonData
//
//  Created by Kok Hung Chan on 20/04/2021.
//

import UIKit

class ViewController: UIViewController {
    
    var getname = String("No color")
    var getcolor = UIColor(ciColor: .white)
    
    @IBOutlet weak var colorsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        colorsLabel.text! = getname
        view.backgroundColor = getcolor
    }
    
    @IBAction func showColors(_ sender: Any) {
        let selectionVC = storyboard?.instantiateViewController(identifier: "SecondViewController") as! SecondViewController
        selectionVC.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(selectionVC, animated: true)
    }
    
}
