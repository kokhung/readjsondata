//
//  SecondViewController.swift
//  ReadJsonData
//
//  Created by Kok Hung Chan on 20/04/2021.
//

import UIKit
import SwiftMessages

class SecondViewController: UIViewController {
    
    let data = ModelData().colors
    
    @IBOutlet weak var colorTable: UITableView!
    
    override func viewDidLoad() {
        initCommon()
    }
    
    func initCommon() {
        initTableConfig()
    }
    
    func initTableConfig() {
        colorTable.dataSource = self
        colorTable.delegate = self
        colorTable.tableFooterView = UIView()
        colorTable.register(UINib.init(nibName: InfoTableViewCell.CellId, bundle: nil), forCellReuseIdentifier: InfoTableViewCell.CellId)
    }
    
    // MARK: Alert function
    
    func showPopup(_ colorName: String) {
        // Instantiate a message view from the provided card view layout. SwiftMessages searches for nib
        let view = MessageView.viewFromNib(layout: .cardView)

        // Theme message elements with the success style.
        view.configureTheme(.success)

        // Add a drop shadow.
        view.configureDropShadow()

        // Set message title, body, and icon. Here, we're overriding the default warning
        view.configureContent(title: "\(colorName)", body: "\(colorName) is selected")
        
        //hide the button
        view.button?.isHidden = true

        // Increase the external margin around the card. In general, the effect of this setting
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)

        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10

        // Show the message.
        SwiftMessages.show(view: view)
    }
    
}

extension SecondViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: InfoTableViewCell.CellId) as! InfoTableViewCell
        cell.colorLabel?.text = data[indexPath.row].color
        cell.backgroundColor = UIColor(hex: data[indexPath.row].value)!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectionVC = storyboard?.instantiateViewController(identifier: "ViewController") as! ViewController
        selectionVC.modalPresentationStyle = .fullScreen
        selectionVC.getname = data[indexPath.row].color
        selectionVC.getcolor = UIColor(hex: data[indexPath.row].value)!
        navigationController?.pushViewController(selectionVC, animated: true)
        
        showPopup(data[indexPath.row].color)
    }
    
}
