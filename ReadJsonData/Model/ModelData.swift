//
//  ModelData.swift
//  ReadJsonData
//
//  Created by Kok Hung Chan on 20/04/2021.
//

import Foundation

public class ModelData {
    
    @Published var colors = [ColorsModel]()
    
    init() { load() }
    
    func load() {
        if let fileLocation = Bundle.main.url(forResource: "colors", withExtension: "json") {

            // do catch in case of an error
            do {
                let data = try Data(contentsOf: fileLocation)
                let jsonDecoder = JSONDecoder()
                let dataFromJson = try jsonDecoder.decode([ColorsModel].self, from: data)
                
                self.colors = dataFromJson
            } catch {
                print(error)
            }
        }
    }
}
