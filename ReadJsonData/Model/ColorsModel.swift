//
//  ColorsModel.swift
//  ReadJsonData
//
//  Created by Kok Hung Chan on 20/04/2021.
//

import Foundation

struct ColorsModel: Codable  {
    var color: String
    var value: String
}

