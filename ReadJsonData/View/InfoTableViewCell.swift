//
//  InfoTableViewCell.swift
//  ReadJsonData
//
//  Created by Kok Hung Chan on 20/04/2021.
//

import UIKit

class InfoTableViewCell: UITableViewCell {

    static let CellId = "InfoTableViewCell"
    
    @IBOutlet weak var colorLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
